package com.dbs.frauddetection.processor.service;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.dbs.frauddetection.dao.LocationDao;
import com.dbs.frauddetection.dao.TransactionDao;
import com.dbs.frauddetection.model.Location;
import com.dbs.frauddetection.processor.dto.TransactionInputDto;
import com.dbs.frauddetection.processor.gateway.VisualizationGateway;

@Component
public class PostProcess {
	private static final Logger _LOGGER = LoggerFactory.getLogger(PostProcess.class);
	
	@Autowired
	private VisualizationGateway visualizationGateway;
	
	@Autowired
	private TransactionDao transactionDao;
	
	@Autowired
	private LocationDao locationDao;
	
	@Async
	public void process(TransactionInputDto dto) {
		//Send to dashboard
		Pageable pageable = new PageRequest(0, 1000);
		Location location = locationDao.findFirst10(pageable).get(new Random().nextInt(1000));
		_LOGGER.info("process - location " + location);
		visualizationGateway.notify(location);
		
		//Store to database
//		transactionDao.save(dto.toEntity());
	}
}
