package com.dbs.frauddetection.visualization.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.frauddetection.cache.FraudLocationCache;
import com.dbs.frauddetection.error.ErrorCode;
import com.dbs.frauddetection.model.Location;

@RestController
@RequestMapping("/notification")
public class NotificationController {

	private static final Logger _LOGGER = LoggerFactory.getLogger(NotificationController.class);
	
	@Autowired
	private FraudLocationCache fraudLocationCache;
	
	@RequestMapping(method = RequestMethod.POST)
	public ErrorCode notify(@RequestBody Location location) {
		_LOGGER.info("notify - location = " + location);
		
		fraudLocationCache.updateNewFrauds(location);
		
		return ErrorCode.OK;
	}
}
