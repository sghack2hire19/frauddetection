package com.dbs.frauddetection.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transaction")
public class Transaction {
	
	@Id
	private Long id;
	
	private Long deviceId;
	
	private BigDecimal transValue;

	private Long accountId;
	
	private Long tsMil;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public BigDecimal getTransValue() {
		return transValue;
	}

	public void setTransValue(BigDecimal transValue) {
		this.transValue = transValue;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getTsMil() {
		return tsMil;
	}

	public void setTsMil(Long tsMil) {
		this.tsMil = tsMil;
	}
	
}
