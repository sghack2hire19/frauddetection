package com.dbs.frauddetection.processor.gateway;

import com.dbs.frauddetection.processor.dto.TransactionInputDto;

public interface MachineLearningGateway {
	boolean isFraud(TransactionInputDto dto);
}
