package com.dbs.frauddetection.processor.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.dbs.frauddetection.error.ErrorCode;
import com.dbs.frauddetection.model.Location;

@Component
public class VisualizationGateway {

	@Value("${visualizationGatewayUrl}")
	private String visualizationGatewayUrl;
	
	@Autowired
	private RestTemplate restTemplate;
	
	public void notify(Location location) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<Location> entity = new HttpEntity<Location>(location,headers);


		restTemplate.exchange(visualizationGatewayUrl, HttpMethod.POST, entity, ErrorCode.class);
	}
}
