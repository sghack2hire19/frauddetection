package com.dbs.frauddetection.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "location")
public class Location {
	
	@Id
	private Long zip;
	
	private Float latitude;
	
	private Float longitude;
	
	private String city;
	
	private String state;
	
	private String county;
	
	private String name;
	
	public Long getZip() {
		return zip;
	}
	
	public void setZip(Long zip){
		this.zip = zip;
	}
	
	public void setLatitude(Float latitude){
		this.latitude = latitude;
		
	}
	public Float getLatitude(){
		return latitude;
	}
	
	public void setLongitude(Float longitude){
		this.longitude = longitude;
		
	}
	public Float getLongitude(){
		return longitude;
	}
	
	public void setCity(String city){
		this.city= city;
	}
	
	public String getCity(){
		return city;
	}
	
	public void setState(String state){
		this.state=state;
	}
	
	public String getState(){
		return state;
	}
	
	
	public void setCounty(String county){
		this.county=county;
	}
	
	public String getCounty(){
		return county;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public String getName(){
		return this.name;
	}

	@Override
	public String toString() {
		return "Location [zip=" + zip + ", latitude=" + latitude + ", longitude=" + longitude + ", city=" + city
				+ ", state=" + state + ", county=" + county + ", name=" + name + "]";
	}
	
}
