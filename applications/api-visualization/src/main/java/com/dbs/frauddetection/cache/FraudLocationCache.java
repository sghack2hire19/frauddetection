package com.dbs.frauddetection.cache;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

import com.dbs.frauddetection.model.Location;

/**
 * Why EHCache cannot work!! CRY!!!
 * @author Andrew
 *
 */
@Component
public class FraudLocationCache {
	
	private ConcurrentHashMap<String, List<Location>> cache = new ConcurrentHashMap<>();
	
//    @Cacheable(value = "newFraud", unless = "#result.isEmpty()")
    public List<Location> getNewFrauds() {
    	List<Location> locations = cache.get("newFraud");
    	if (locations == null) {
    		locations = new ArrayList<>();
    	}
    	return locations;
	}
    
//    @CachePut(value = "newFraud", key = "#input.zip")
    public void updateNewFrauds(Location input) {
    	List<Location> locations = cache.get("newFraud");
    	if (locations == null) {
    		locations = new ArrayList<>();
    	}
    	locations.add(input);
    	cache.put("newFraud", locations);
	}
}
