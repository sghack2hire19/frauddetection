package com.dbs.frauddetection;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootConfiguration
@ComponentScan("com.dbs.frauddetection")
@Configuration
public class BaseTest {

}
