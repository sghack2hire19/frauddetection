package com.dbs.frauddetection.analysis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dbs.frauddetection.dao.TransactionDao;

@Component
public class FraudDetectionProcessor {
	
	@Autowired
	private TransactionDao transactionDao;
	
	public boolean isFraud() {
		return false;
	}
}
