package com.dbs.frauddetection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import com.dbs.frauddetection.analysis.FraudDetectionProcessor;


@RunWith(SpringRunner.class)
@SpringBootApplication
@SpringBootTest
@ComponentScan(basePackages = "com.dbs.frauddetection")
public class FraudDetectionProcessorTest {

	@Autowired
	private FraudDetectionProcessor fraudDetectionProcessor;
	
	@Test
	public void testIsFraud() {
		Assert.assertEquals(false, fraudDetectionProcessor.isFraud());
	}
}
