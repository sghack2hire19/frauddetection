package com.dbs.frauddetection.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.dbs.frauddetection.model.Transaction;

@Transactional
public interface TransactionDao extends CrudRepository<Transaction, Long>  {
	Transaction findById(Long txnId);
}
