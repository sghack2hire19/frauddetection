package com.dbs.frauddetection.visualization.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dbs.frauddetection.cache.FraudLocationCache;
import com.dbs.frauddetection.dao.LocationDao;
import com.dbs.frauddetection.model.Location;

@Controller
@RequestMapping("/dashboardBak")
public class DashboardControllerBak {

	@Autowired
	private LocationDao locationDao;
	
	@Autowired
	private FraudLocationCache fraudLocationCache;
	
	@RequestMapping(path = "/index", method = RequestMethod.GET)
	public String dashboard() {
		return "dashboard";
	}
	
	@RequestMapping(path = "/updates", method = RequestMethod.GET)
	public @ResponseBody List<Location> getUpdates() {
		List<Location> locations = new ArrayList<>();
//		Location l1 = new Location();
//		l1.setName("New Hampshire");
//		l1.setState("NH");
//		l1.setLatitude(43.524872f);
//		l1.setLongitude(-71.445841f);
//		locations.add(l1);
//		
//		Location l2 = new Location();
//		l2.setName("New Jersey");
//		l2.setState("NJ");
//		l2.setLatitude(40.582845f);
//		l2.setLongitude(-74.27524f);
//		locations.add(l2);
//		
//		//27.976883	-81.614414
//		Location l3 = new Location();
//		l3.setState("FL");
//		l3.setName("Florida");
//		l3.setLatitude(27.976883f);
//		l3.setLongitude(-81.614414f);
//		locations.add(l3);
		
//		Pageable topTen = new PageRequest(0, 500);
//		locations = locationDao.findFirst10(topTen);
		
//		Iterator<Location> iter = locationDao.findAll().iterator();
//		while (iter.hasNext()) {
//			locations.add(iter.next());
//		}   
		
		locations = fraudLocationCache.getNewFrauds();
		return locations;
	}
	
	@RequestMapping(path = "/push", method = RequestMethod.GET)
	public @ResponseBody List<Location> push() {
		List<Location> locations = new ArrayList<>();
//		Random random = new Random(100);
//		Location l1 = new Location();
//		l1.setCity("City " + random.nextInt());
//		l1.setName("New Hampshire");
//		l1.setState("NH");
//		l1.setLatitude(random.nextFloat());
//		l1.setLongitude(-random.nextFloat());
//		locations.add(l1);
		
//		Pageable topTen = new PageRequest(0, 5000);
//		List<Location> list = locationDao.findFirst10(topTen);
//		locations.add(list.get(new Random().nextInt(5000)));
//		locations.add(list.get(new Random().nextInt(5000)));
//		locations.add(list.get(new Random().nextInt(5000)));
		
		return fraudLocationCache.getNewFrauds();
	}
}
