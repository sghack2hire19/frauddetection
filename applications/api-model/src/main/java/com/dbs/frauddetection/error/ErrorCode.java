package com.dbs.frauddetection.error;

public class ErrorCode {
	
	public static final ErrorCode OK = new ErrorCode(200, "OK");
	
	public static final ErrorCode NOK = new ErrorCode(200, "NOK");
	
	private int code;
	
	private String message;
	
	public ErrorCode() {
		
	}

	public ErrorCode(int code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
