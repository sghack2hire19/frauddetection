package com.dbs.frauddetection.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.dbs.frauddetection.model.Location;

@Transactional
public interface LocationDao extends CrudRepository<Location, Long>  {
	@Query("from Location")
	List<Location> findFirst10(Pageable pageable);
}