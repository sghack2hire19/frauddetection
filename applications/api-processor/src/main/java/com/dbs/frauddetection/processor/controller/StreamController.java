package com.dbs.frauddetection.processor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.frauddetection.error.ErrorCode;
import com.dbs.frauddetection.processor.dto.TransactionInputDto;
import com.dbs.frauddetection.processor.gateway.MachineLearningGateway;
import com.dbs.frauddetection.processor.service.PostProcess;

@RestController
@RequestMapping("/stream")
public class StreamController {

	private MachineLearningGateway machineLearningGateway;
	
	@Autowired
	private PostProcess postProcess;
	
	@RequestMapping(method = RequestMethod.POST)
	public ErrorCode processTxn(TransactionInputDto input) {
//		if (!machineLearningGateway.isFraud(input)) {
//			return ErrorCode.NOK;
//		}	
		
		//Async
		postProcess.process(input);
		
		return ErrorCode.OK;
	}
}
