package com.dbs.frauddetection.visualization.controller;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dbs.frauddetection.cache.FraudLocationCache;
import com.dbs.frauddetection.dao.LocationDao;
import com.dbs.frauddetection.model.Location;

@Controller
@RequestMapping("/dashboard")
public class DashboardController {
	
	private static final Logger _LOGGER = LoggerFactory.getLogger(DashboardController.class);

	@Autowired
	private LocationDao locationDao;
	
	@Autowired
	private FraudLocationCache fraudLocationCache;
	
	@RequestMapping(path = "/index", method = RequestMethod.GET)
	public String dashboard() {
		return "dashboard";
	}
	
	@RequestMapping(path = "/init", method = RequestMethod.GET)
	public @ResponseBody List<Location> init() {

		Pageable topTen = new PageRequest(0, 1);
		List<Location> locations = locationDao.findFirst10(topTen);
		
//		Iterator<Location> iter = locationDao.findAll().iterator();
//		while (iter.hasNext()) {
//			locations.add(iter.next());
//		}   
		return locations;
	}
	
	@RequestMapping(path = "/updates", method = RequestMethod.GET)
	public @ResponseBody List<Location> push() {
		List<Location> locations = fraudLocationCache.getNewFrauds();
		_LOGGER.info("locations = " + locations);
		return locations;
	}
}
