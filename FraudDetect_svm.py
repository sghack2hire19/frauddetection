# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
from __future__ import division

from sklearn import svm
from sklearn.externals import joblib
import pandas as pd
import random

import cPickle

from .svmpy import SVMTrainer

trans = pd.read_csv("transactions_fromR.csv")
device = pd.read_csv("pos_device.csv")

idx_train = random.sample(trans.index, int(trans.shape[0]*0.75))
idx_test = list(set(trans.index) -set(idx_train))
train_df = trans.ix[idx_train].loc[:, ('transaction_value','account_id','device_id')].as_matrix()

test_df = trans.ix[idx_test].loc[:, ('transaction_value','account_id','device_id')].as_matrix()


clf = svm.SVC(kernel='rbf', C=1.0)
clf.fit(train_df, trans.ix[idx_train]['fraud'])

pred_test = clf.predict(test_df.loc[:, ('transaction_value','account_id','device_id')])


accuracy = sum(pred_test==test_df.fraud)/len(pred_test)

with open('svmModel.pkl','wb') as f:
    cPickle.dump(clf, f)



with open('svmModel.pkl','rb') as f:
    clf_loaded = cPickle.load(f)
    
    
#import svmpy
#import sys
#sys.path.append('./svmpy')
#import SVMTrainer
#import SVMTrainer, kernel
#k = kernel.Kernel.linear()
#trainer = SVMTrainer(k, 0.1)
train_df.loc[:, ('transaction_value','account_id','device_id')].as_matrix()
