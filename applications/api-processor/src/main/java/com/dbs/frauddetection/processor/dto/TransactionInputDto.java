package com.dbs.frauddetection.processor.dto;

import java.math.BigDecimal;

import com.dbs.frauddetection.model.Transaction;

public class TransactionInputDto {
	
	private Long deviceId;
	
	private String accountId;
	
	private BigDecimal value;
	
	private Long tsMil;

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public Long getTsMil() {
		return tsMil;
	}

	public void setTsMil(Long tsMil) {
		this.tsMil = tsMil;
	}
	
	public Transaction toEntity() {
		return new Transaction();
	}
}
