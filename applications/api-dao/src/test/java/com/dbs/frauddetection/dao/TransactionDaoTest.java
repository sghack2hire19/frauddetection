package com.dbs.frauddetection.dao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import com.dbs.frauddetection.model.Transaction;

@RunWith(SpringRunner.class)
@SpringBootApplication
@ComponentScan(basePackages = "com.dbs.frauddetection")
@EntityScan(basePackages = "com.dbs.frauddetection.model")
@SpringBootTest
public class TransactionDaoTest {
	
	@Autowired
	private TransactionDao dao;
	
	@Test
	public void testFindById() {
		Transaction txn = dao.findById(12L);
		Assert.assertNull(txn);
	}
}
